import logo from './logo.svg';
import './App.css';
import HelloWorld from './components/HelloWorld'
import SayMyName from './components/SayMyName'
import Pessoa from './components/Pessoa'
import Frase from './components/Frase'
import List from './components/List'
import Evento from "./components/Evento"
import Form from "./components/Form"
import Condicional from "./components/Condicional";
import OutraLista from "./components/OutraLista";
import {useState} from "react";
import SeuNome from "./components/SeuNome";
import Saudacao from "./components/Saudacao";
import {
    BrowserRouter as Router,
    Routes,
    Link, Route
} from "react-router-dom";
import Contato from "./pages/Contato";
import Empresa from "./pages/Empresa";
import Home from "./pages/Home";
import Navbar from "./components/layout/Navbar";
import Footer from "./components/layout/Footer";

// JSX
function App() {
    /**
        const name = 'Bruno';
        const newName = name.toUpperCase()

        function sum(a, b) {
            return a + b
        }

        const url = "https://via.placeholder.com/150"


        const meusItens = ['React', 'Vue', 'Angular']
        const [nome, setNome] = useState()
    **/
    return (
            /**
                 <div className="App">
                 <h1>Alterando o JSX</h1>
            <p>Olá, {newName}</p>
            <p>Soma: {sum(2,2)}</p>
            <img src={url} alt="Minha imagem"/>
             <HelloWorld/>

                         <Frase/>
            <Frase/>
            <SayMyName nome="Heisenberg"/>
            <SayMyName nome={nome}/>
            <Pessoa
                nome='Bruno'
                idade='24'
                profissao='Estagiário'
                foto='https://via.placeholder.com/150'
            />
            <List/>
            <Evento/>

            <Condicional/>
             <Form/>
                         <h1>State Lift</h1>
            <SeuNome setNome={setNome}/>
            <Saudacao nome={nome}/>
        </div>
            **/
        <Router>
            <Navbar/>
            <Routes>
                <Route exact path="/" element={<Home/>}></Route>
                <Route path="/Contato" element={<Contato/>}></Route>
                <Route path="/Empresa" element={<Empresa/>}></Route>
            </Routes>
            <Footer/>
        </Router>
    );
}

export default App;
