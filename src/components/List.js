import Item from './Item'

function List(){
    return(
        <>{/*Fragmento*/}
            <h1>Minha Lista</h1>
            <ul>
                <Item marca="Ferrari" ano_lancamento={1985}/>
                <Item marca="Fiat" ano_lancamento={1964}/>
                <Item marca="Renault" ano_lancamento={1990}/>
            </ul>
        </>
    )
}

export default List