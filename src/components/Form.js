//setName atribui o valor e name busca o valor
import {useState} from "react";

function Form(){
    function cadastrarUsuario(e){
        e.preventDefault()// para o envio do formulario ao servidor e executa o código a baixo
        console.log(`Usuário ${name} foi cadastrado com a senha: ${password}`)
        console.log('Cadastrou o Usuário!')
    }
    //nome do atributo e o que vai ser alterado
    const [name, setName] = useState() // <valor default
    const [password, setPassword] = useState()


    return(
        <div>
            <h1>Meu Cadastro:</h1>
            <form onSubmit={cadastrarUsuario}>
                <div>
                    <label htmlFor="name">Nome: </label>
                    <input type="text"
                           id="name"
                           name="name"
                           placeholder="Digite o seu nome"
                        //quando clicar no botão  ele enviar o valor digitado
                           onChange={(e) => setName(e.target.value)}
                    />
                </div>

                <div>
                    <label htmlFor="password">Senha: </label>
                    <input type="password"
                           id="password"
                           name="password"
                           placeholder="Digite a sua senha"
                           onChange={(e) => setPassword(e.target.value)}
                    />
                </div>
                <div>
                    <input type="submit" placeholder="Cadastrar"/>
                </div>
            </form>
        </div>
    )
}

export default Form