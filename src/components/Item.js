import PropTypes from 'prop-types'
function Item({marca,ano_lancamento}){
    return(
        <>
            <li>{marca} - {ano_lancamento}</li>

        </>
    )
}
//Define o tipo das propriedades
Item.propTypes = {
    marca: PropTypes.string.isRequired, //tem que ser string e é obrigatório
    ano_lancamento: PropTypes.number.isRequired,
}
//Define valores default para o item
Item.defaultProps = {
    marca: 'Falta a Marca',
    ano_lancamento: 0,
}
export default Item